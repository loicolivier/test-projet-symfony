<?php
/**
 * Model base class
 */
namespace Model\Region;

class Region {
	private $name;
	// private $code_region;
	// private $departements=[];

	function __construct($name)
	{
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	// public function getCodeRegion() {
	// 	return $this->region;
	// }
	// public function setCodeRegion($regionCode) {
	// 	$this->region = $regionCode;
	// }

}