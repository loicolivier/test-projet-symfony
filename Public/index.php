<?php declare(strict_types=1);
include '../vendor/autoload.php';

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Controller\controller;

$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILESgit 
);

$router = new League\Route\Router; 


$router->map('GET', '/', [controller::class, 'homepage']);
//$router->map('GET', '/tests', [controller::class, 'method']);
$router->map('GET', '/list', [controller::class, 'listing']);


$response = $router->dispatch($request);


(new Laminas\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);
